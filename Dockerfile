FROM python:3.9

# ENV PATH="/scripts:${PATH}"

ENV PYTHONDONTWRITEBYTECODE 1

ENV PYTHONUNBUFFERED 1

WORKDIR /code

COPY . /code/

RUN pip install --upgrade pip

RUN pip3 install -r /code/requirements.txt

RUN chmod +x entrypoint.sh

RUN chmod +x migration.sh


CMD ["./entrypoint.sh"]