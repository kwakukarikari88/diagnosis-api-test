# mpharma diagnosis api setup

## Docker Setup
Run docker to build the image
```sh
$ docker build .
```
After the build Run to start the app
```sh
$ docker-compose up -d 
```
Run docker ps to confirm 
```sh
$ docker ps
```
Run migrations and upload initial data
```sh
$ docker exec -it app ./migration.sh
```
Runing Unit Test
```sh
$ docker exec -it app python3 manage.py test
```
## Api Docs
### List all diagnosis
```
  GET ->  http://127.0.0.1:8000/api/v1/diagnosis/ 
```
### Save diagnosis
```
    POST ->  http://127.0.0.1:8000/api/v1/diagnosis/
    data : {
        "category_code": "B50",
        "diagnosis_code": "0",
        "full_code": "B500",
        "abbreviated_description": "Plasmodium falciparum malaria with cerebral complications",
        "full_description": "Plasmodium falciparum malaria with cerebral complications",
        "title": "Plasmodium falciparum malaria"
    }
```
### Fetch a diagnosis by ID
```
  GET ->  http://127.0.0.1:8000/api/v1/id{uuid}/
```

### Update a diagnosis by ID
```
  PUT ->  http://127.0.0.1:8000/api/v1/id{uuid}/
  data : {
        "category_code": "B50",
        "diagnosis_code": "0",
        "full_code": "B500",
        "abbreviated_description": "Plasmodium falciparum malaria with cerebral complications",
        "full_description": "Plasmodium falciparum malaria with cerebral complications",
        "title": "Plasmodium falciparum malaria"
    }
```
### Delete a diagnosis by ID
```
   DELETE ->  http://127.0.0.1:8000/api/v1/id{uuid}/
```
### Incase Diagnosis codes get updated
```
GET -> http://127.0.0.1:8000/api/v1/download-diagnosis/
```
