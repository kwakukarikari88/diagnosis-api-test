from django.db import models
import uuid

# Create your models here.


class Diagnosis(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=True)
    category_code = models.CharField(max_length=25, db_index=True)
    diagnosis_code = models.CharField(max_length=25, null=True, default="")
    full_code = models.CharField(max_length=25, default="", unique=True, db_index=True)
    abbreviated_description = models.CharField(max_length=225)
    full_description = models.CharField(max_length=500)
    title = models.CharField(max_length=225)

    class Meta:
        ordering = ['category_code']

    def __str__(self):
        return "{} {}".format(self.full_code, self.full_description)
