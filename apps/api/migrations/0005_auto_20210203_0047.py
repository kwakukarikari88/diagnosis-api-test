# Generated by Django 3.1.5 on 2021-02-03 00:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20210203_0039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='diagnosis',
            name='category_code',
            field=models.CharField(db_index=True, max_length=25),
        ),
        migrations.AlterField(
            model_name='diagnosis',
            name='full_code',
            field=models.CharField(db_index=True, default='', max_length=25, unique=True),
        ),
    ]
