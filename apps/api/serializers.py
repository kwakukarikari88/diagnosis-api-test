from rest_framework import serializers
from . models import Diagnosis


class DiagnosisSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)

    class Meta:
        model = Diagnosis
        fields = "__all__"
