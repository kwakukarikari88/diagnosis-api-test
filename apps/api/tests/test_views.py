from . test_setup import TestSetUp
from apps.api.models import Diagnosis


class TestViews(TestSetUp):

    def test_diagnosis_cannot_be_saved_with_no_data(self):
        res = self.client.post(self.diagnosis_url, data={})
        self.assertEqual(res.status_code, 400)

    def test_diagnosis_can_be_save_with_data(self):
        res = self.client.post(self.diagnosis_url, self.data)
        self.assertEqual(res.status_code, 201)

    def test_diagnosis_fetch_all_data(self):
        res = self.client.get(self.diagnosis_url, format='json')
        self.assertEqual(len(res.data), 4)

    def test_diagnosis_retrieve_item_by_id(self):
        url = self.diagnosis_url + "f5136723-f92a-42b4-8496-3936d5df739e/"
        res = self.client.get(url, format='json')
        data = res.data['data']
        self.assertEqual(data['category_code'], 'A00')
        self.assertEqual(data['diagnosis_code'], '0')
        self.assertEqual(data['full_code'], 'A000')
        self.assertEqual(data['title'], 'Cholera')
        self.assertEqual(res.status_code, 200)

    def test_update_of_diagnosis_data(self):
        url = self.diagnosis_url + "f5136723-f92a-42b4-8496-3936d5df739e/"
        res = self.client.put(url, data=self.data, format='json')
        data = res.data['data']
        self.assertEqual(data['category_code'], 'Z00')
        self.assertEqual(data['full_code'], 'AZ000')
        self.assertEqual(res.status_code, 200)

    def test_cant_update__with_no_data(self):
        url = self.diagnosis_url + "f5136723-f92a-42b4-8496-3936d5df739e/"
        res = self.client.put(url, data={}, format='json')
        self.assertEqual(res.status_code, 400)

    def test_delete_of_diagnosis_data(self):
        url = self.diagnosis_url + "f5136723-f92a-42b4-8496-3936d5df739e/"
        res = self.client.delete(url, format=format)
        d = Diagnosis.objects.all()
        self.assertNotEqual(len(d), 3)
        self.assertEqual(res.status_code, 204)

    def test_diagnosis_not_found_by_ID(self):
        url = self.diagnosis_url + "a5136723-f92a-42b4-8496-3936d5df739s/"
        res = self.client.delete(url, format=format)
        self.assertEqual(res.status_code, 404)
