from rest_framework.test import APITestCase
from django.urls import reverse
from apps.api.models import Diagnosis


class TestSetUp(APITestCase):
    pass

    def setUp(self):
        # self.diagnosis_url_save = reverse('api:diagnosis')
        self.diagnosis_url = reverse('api:diagnosis')

        self.data = {"category_code": "Z00",
                     "diagnosis_code": "0",
                     "full_code": "AZ000",
                     "abbreviated_description": "Cholera due to Vibrio cholerae 01, biovar cholerae",
                     "full_description": "Cholera due to Vibrio cholerae 01, biovar cholerae",
                     "title": "Cholera"
                     }

        self.data_list = [{"id": "f5136723-f92a-42b4-8496-3936d5df739e",
                           "category_code": "A00",
                           "diagnosis_code": "0",
                           "full_code": "A000",
                           "abbreviated_description": "Cholera due to Vibrio cholerae 01, biovar cholerae",
                           "full_description": "Cholera due to Vibrio cholerae 01, biovar cholerae",
                           "title": "Cholera"
                          },
                          {
                            "id": "6376df08-b956-46a1-9e2f-08cff5ea76d1",
                            "category_code": "A00",
                            "diagnosis_code": "1",
                            "full_code": "A001",
                            "abbreviated_description": "Cholera due to Vibrio cholerae 01, biovar eltor",
                            "full_description": "Cholera due to Vibrio cholerae 01, biovar eltor",
                            "title": "Cholera"
                        },
                        {
                            "id": "e1cd1b2a-5131-4467-a750-bee6d0dfd1ac",
                            "category_code": "A00",
                            "diagnosis_code": "9",
                            "full_code": "A009",
                            "abbreviated_description": "Cholera, unspecified",
                            "full_description": "Cholera, unspecified",
                            "title": "Cholera"
                        }
                    ]

        for row in self.data_list:
            Diagnosis.objects.create(id=row['id'],
                                     category_code=row['category_code'],
                                     diagnosis_code=row['diagnosis_code'],
                                     full_code=row['full_code'],
                                     abbreviated_description=row['abbreviated_description'],
                                     full_description=row['full_description'],
                                     title=row['title'])

        return super().setUp()

    def tearDown(self):
        return super().tearDown()
