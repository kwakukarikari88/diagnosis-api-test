from django.test import TestCase
from apps.api.models import Diagnosis

class DiagnosisModelTest(TestCase):

    @classmethod
    def setUp(self):
       self.diagnosis = Diagnosis.objects.create(category_code="A00",
                                                 diagnosis_code="0",
                                                 full_code="A000",
                                                 abbreviated_description="Cholera due to Vibrio cholerae 01, biovar cholerae",
                                                 full_description="Cholera due to Vibrio cholerae 01, biovar cholerae",
                                                 title="Cholera")

    def test_dignosis_model(self):
        self.assertTrue(isinstance(self.diagnosis, Diagnosis))

    def test_diagnosis_categoy_code(self):
        self.assertEqual(self.diagnosis.category_code, "A00")
