from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.decorators import api_view
from django_filters.rest_framework import DjangoFilterBackend
from . models import Diagnosis
from . serializers import DiagnosisSerializer
from . paginate import CustomPagination
import pandas as pd
import csv
import requests
from contextlib import closing
import concurrent.futures
import time


# Create your views here.


class DiagnosisAPIView(APIView):
    serializer_class = DiagnosisSerializer

    def get(self, request):

        paginator = CustomPagination()

        queryset = Diagnosis.objects.all()

        query_data = paginator.paginate_queryset(queryset, request)
        serializer = self.serializer_class(query_data, many=True)
        return paginator.get_paginated_response(serializer.data)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            res = {'data': serializer.data, 'message': 'Diagnosis code saved successfully'}
            return Response(res, status=status.HTTP_201_CREATED)
        else:
            res = {'errors': serializer.errors}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)


class DiagnosisDetailAPIView(APIView):
    serializer_class = DiagnosisSerializer

    def get_object(self, id):
        return get_object_or_None(Diagnosis, pk=id)

    def get(self, request, id):
        disgnosis = self.get_object(id)
        if disgnosis is None:
            res = {'message': 'Diagnosis not found'}
            return Response(res, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(disgnosis)
        res = {'data': serializer.data}
        return Response(res, status=status.HTTP_200_OK)

    def put(self, request, id):
        diagnosis = self.get_object(id)
        if diagnosis is None:
            res = {'message': 'Diagnosis not found'}
            return Response(res, status=status.HTTP_404_NOT_FOUND)

        serializer = self.serializer_class(diagnosis, request.data)

        if serializer.is_valid():
            serializer.save()
            res = {'data': serializer.data, 'message': 'Diagnosis updated successfully'}
            return Response(res, status=status.HTTP_200_OK)
        else:
            res = {'errors': serializer.errors}
            return Response(res, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        diagnosis = self.get_object(id)
        diagnosis.delete()
        res = {'message': 'Diagnosis delete successfully'}
        return Response(res, status=status.HTTP_204_NO_CONTENT)


class DiagnosisListAPIView(ListAPIView):
    queryset = Diagnosis.objects.all()
    serializer_class = DiagnosisSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['full_code', 'category_code', 'title']


@api_view(['GET', ])
def uploadDiagnosisData(request):
    try:
        url = "https://raw.githubusercontent.com/kamillamagna/ICD-10-CSV/master/codes.csv"

        with closing(requests.get(url, stream=True)) as r:
            f = (line.decode('utf-8') for line in r.iter_lines())
            reader = csv.reader(f, delimiter=',', quotechar='"')
            for row in reader:
                # print(row)
                diag = get_diagnosis_by_full_code_or_None(Diagnosis, full_code=row[2])
                if diag is None:
                    Diagnosis.objects.create(category_code=row[0],
                                             diagnosis_code=row[1],
                                             full_code=row[2],
                                             abbreviated_description=row[3],
                                             full_description=row[4],
                                             title=row[5])
                else:
                    diag.category_code = row[0]
                    diag.diagnosis_code = row[1]
                    diag.full_code = row[2]
                    diag.abbreviated_description = row[3]
                    diag.full_description = row[4]
                    diag.title = row[5]
                    diag.save()

    except Exception as e:
        res = {'statusCode': '500_ERROR', 'message': str(e)}
        return Response(res, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    res = {'statusCode': '200_OK', 'message': 'Data Uploaded Successfully'}
    return Response(res, status=status.HTTP_200_OK)


@api_view(['GET', ])
def uploadDiagnosisDataPanda(request):
    try:
        url = "https://raw.githubusercontent.com/kamillamagna/ICD-10-CSV/master/codes.csv"
        df = pd.read_csv(url, dtype=object)
        d_list = df.values.tolist()

        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(saveDiagnosis, d_list)

    except Exception as e:
        res = {'statusCode': '500_ERROR', 'message': str(e)}
        return Response(res, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    res = {'statusCode': '200_OK', 'message': "Data Uploaded Successfully"}
    return Response(res, status=status.HTTP_200_OK)


def saveDiagnosis(row):
    diag = get_diagnosis_by_full_code_or_None(Diagnosis, full_code=row[2])
    if diag is None:
        Diagnosis.objects.create(category_code=row[0],
                                 diagnosis_code=row[1],
                                 full_code=row[2],
                                 abbreviated_description=row[3],
                                 full_description=row[4],
                                 title=row[5])
    else:
        diag.category_code = row[0]
        diag.diagnosis_code = row[1]
        diag.full_code = row[2]
        diag.abbreviated_description = row[3]
        diag.full_description = row[4]
        diag.title = row[5]
        diag.save()


def get_object_or_None(model_class, pk):
    try:
        result = model_class.objects.get(pk=pk)
    except model_class.DoesNotExist:
        result = None

    return result


def get_diagnosis_by_full_code_or_None(model_class, full_code):
    try:
        result = model_class.objects.get(full_code=full_code)
    except model_class.DoesNotExist:
        result = None

    return result
