from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . views import *


app_name = 'api'

# /api/v1/
urlpatterns = [

    path('diagnosis/', DiagnosisAPIView.as_view(), name='diagnosis'),    # POST  # GET

    # path("diagnosis/", DiagnosisListAPIView.as_view(), name="diagnosis"),  # GET

    path('diagnosis/<uuid:id>/', DiagnosisDetailAPIView.as_view(), name="diagnosis-details"),    # GET, PUT, DELETE

    path('download-diagnosis/', uploadDiagnosisDataPanda, name='upload-diagnosis-data')  # GET

]

urlpatterns = format_suffix_patterns(urlpatterns)
